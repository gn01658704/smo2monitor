package com.bkeen.smo2monitor

import android.util.Log

public class MedianFilter {

    //Quicksort
    fun qsort(array: IntArray, start: Int, end: Int) {
        var i = start
        var j = end
        //choosing pivot element
        val pivot = array[(start + end) / 2]

        do {
            //searching for element to swap
            while (array[i] < pivot) i++
            while (array[j] > pivot) j--
            if (i <= j) {
                //swap
                val temp = array[i]
                array[i] = array[j]
                array[j] = temp
                i++
                j--
            }
        } while (i <= j)

        //if have parts that contains more than 2 elements, we use the recursive sorting one more time for each part
        if (j > start) qsort(array, start, j)
        if (i < end) qsort(array, i, end)


    }

    //this method finds the central element of sorted array
    fun findLocalMedian(oldArray: IntArray, start: Int, end: Int): Int {
        val result = IntArray(end - start)
        for (i in 0..result.size - 1) {
            result[i] = oldArray[start + i]
        }
        qsort(result, 0, result.size - 1)
        return result[(result.size - 1) / 2]
    }

    //this method modifies input sequence of numbers, filling the gaps to treat the edges
    private fun modifyInput(oldArray: IntArray, window: Int): IntArray {
        val gapSize = window % ((window + 1) / 2)      //finding the gap size at each side
        val result = IntArray(oldArray.size + 2 * gapSize)
        //fill the gaps
        for (i in 0 until gapSize) {
            result[i] = oldArray[0]
            result[result.size - 1 - i] = oldArray[oldArray.size - 1]
        }
        //copying the other part of input array
        for (i in gapSize until result.size - gapSize) {
            result[i] = oldArray[i - gapSize]
        }
        return result
    }

    //this method creates the result filtered array of medians for each sequence
    public fun filterSequence(oldArray: IntArray, window: Int): IntArray {
        var oldArray = oldArray
        val result = IntArray(oldArray.size)
        oldArray = modifyInput(oldArray, window)
        for (i in result.indices) {
            result[i] = findLocalMedian(oldArray, i, i + window)
        }
        return result
    }
}