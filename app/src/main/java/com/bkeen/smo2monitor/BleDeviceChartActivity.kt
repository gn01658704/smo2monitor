package com.bkeen.smo2monitor


import android.bluetooth.*
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.*
import android.widget.SeekBar.OnSeekBarChangeListener
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.aachartmodel.aainfographics.AAInfographicsLib.AAChartCreator.*
import com.aachartmodel.aainfographics.AAInfographicsLib.AAOptionsModel.AAOptions
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.customview.getCustomView
import com.juul.able.experimental.ConnectGattResult
import com.juul.able.experimental.Gatt
import com.juul.able.experimental.GattConnectionLost
import com.juul.able.experimental.android.connectGatt
import com.juul.able.experimental.throwable.writeCharacteristicOrThrow
import com.juul.able.experimental.writeCharacteristic
import com.swallowsonny.convertextlibrary.writeInt16BE
import com.swallowsonny.convertextlibrary.writeInt8
import kotlinx.coroutines.*
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean

class BleDeviceChartActivity : AppCompatActivity() {

    private val TAG = "BleDeviceChartActivity"

    companion object {
        var data_count = 100
        val mLedSettings = listOf(LEDSetting("RED Led", 1), LEDSetting("IR Led", 2))
    }

    private val SERVICE_UUID = UUID.fromString("0000fff0-0000-1000-8000-00805f9b34fb")

    private val CHARACTERISTIC_READ_UUID = UUID.fromString("0000fff2-0000-1000-8000-00805f9b34fb")

    private val CHARACTERISTIC_WRITE_UUID = UUID.fromString("0000fff3-0000-1000-8000-00805f9b34fb")

    private val CHARACTERISTIC_NOTI_UUID = UUID.fromString("0000fff4-0000-1000-8000-00805f9b34fb")

    private val DESCRIPTOR_UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb")

    private var aaChartView: AAChartView? = null
    private lateinit var mSelectChartView: Spinner
    private lateinit var mStopButton: Button
    private lateinit var bluetoothDevice: BluetoothDevice
    private lateinit var mBluetoothGatt: Gatt
    private lateinit var mBluetoothWrite: BluetoothGattCharacteristic
    private lateinit var mBluetoothRead: BluetoothGattCharacteristic
    private var isconnect = false
    private var changeChart = AtomicBoolean(false)
    private lateinit var mContext: Context
    private lateinit var connectJod: Job
    private lateinit var notifyJod: Job

    lateinit var mViewModel: BleDeviceChartViewModel
    var aaChartModel = AAChartModel()

    var dcfg_org_1080 = intArrayOf(
        // 0x00100001,
        0x00113131,
        // 0x00120035,
        0x00140556,
        // 0x00150550,
        0x00181E78,
        0x00191E78,
        0x001A1E78,
        0x001B1E78,
        0x001E1E78,
        0x001F1E78,
        0x00201E78,
        0x00211E78,
        // 0x00243001,
        0x00300321,
        0x00310818,
        0x003922FF,//2017
        0x00233001,
        0x00350321,
        0x00360818,
        0x003B22F7, //2017
        0x0025301F,
        // 0x00340000,..
        // 0x00380000,..
        // 0x00063000,..
        // 0x000100FF,..
        // 0x00020005, // Set Gpio
        0x003C3206,
        0x00421C77,
        0x00441C77,
        0x0043ADA5,
        0x0045ADA5,
        0x00550FFF,
        0x00540020,
        0x005F0001,
        // 0x00243008,
        // 0x0023300C,
        0x00241000,
        0x00233000,
        0x00100002
    )

    internal var count = 0

    class LEDSetting {

        constructor(name: String, number: Int) {
            this.name = name
            this.number = number
        }

        var number = 0
        var name = ""
        var coarse = 2
        var scale = 1
        var fine = 0
    }

    private var cal_v1 = 5500

    suspend fun writeCmdToCharacteristic(charact: BluetoothGattCharacteristic, data: ByteArray) {
        try {
            mBluetoothGatt.writeCharacteristic(charact, data)
        } catch (e: com.juul.able.experimental.GattConnectionLost) {

        }
    }

    suspend fun connectAndConfig(context: Context, device: BluetoothDevice): Boolean {
        mBluetoothGatt = device.connectGatt(context, autoConnect = false).let { result ->
            when (result) {
                is ConnectGattResult.Success -> result.gatt
                is ConnectGattResult.Canceled -> throw IllegalStateException(
                    "Connection Canceled.",
                    result.cause
                )
                is ConnectGattResult.Failure -> throw IllegalStateException(
                    "Connection failed.",
                    result.cause
                )
            }
        }

        connectJod = GlobalScope.launch {
            val state = mBluetoothGatt.onConnectionStateChange.openSubscription()
            for (element in state) {
                Log.d("onConnectionStateChange", "onConnectionStateChange: $element")
                if (element.newState == BluetoothProfile.STATE_DISCONNECTED) {
                    mViewModel.closeFile()
                    mViewModel.stopSaveSmo2()
                }
            }
        }

        if (mBluetoothGatt.discoverServices() != BluetoothGatt.GATT_SUCCESS) {
            return false
        }

        mViewModel.createFile()

        val characteristic =
            mBluetoothGatt.getService(SERVICE_UUID)!!.getCharacteristic(CHARACTERISTIC_NOTI_UUID)
        mBluetoothWrite =
            mBluetoothGatt.getService(SERVICE_UUID)!!.getCharacteristic(CHARACTERISTIC_WRITE_UUID)
        mBluetoothRead =
            mBluetoothGatt.getService(SERVICE_UUID)!!.getCharacteristic(CHARACTERISTIC_READ_UUID)

        mBluetoothGatt.writeDescriptor(
            characteristic.getDescriptor(DESCRIPTOR_UUID),
            BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
        )
        mBluetoothGatt.setCharacteristicNotification(characteristic, true)


        notifyJod = GlobalScope.launch {
            val noti = mBluetoothGatt.onCharacteristicChanged.openSubscription()
            for (element in noti) {
//                Log.i("Gesture", "for updateView")
                updateView(element.value)
            }
        }

        Log.d("ble", "start config reg")
        for (cmd in dcfg_org_1080) {
            var data = ByteArray(4)
            data.writeInt8(cmd shr 16, 1)
            data.writeInt16BE(cmd and 0xffff, 2)
            writeCmdToCharacteristic(mBluetoothWrite, data)
        }


        var result = try {
            mBluetoothGatt.readCharacteristic(mBluetoothRead)
        } catch (e: GattConnectionLost) {
            return false
        }

        var last_value = 0 //3300
        var now_value = 0 //3300

        var led1 = mLedSettings.get(0)
        var led2 = mLedSettings.get(1)

        Log.d("ble", "start config red pd")
        var min_setting = 0
        var max_setting = 0
        var ir_max_value = cal_v1
        while (true) {
            var _rowdata = result.value

            var mPDSensor = BleDeviceChartViewModel.PDSensor(_rowdata)
//            val pd7 = (_rowdata[12].toInt() and 0xFF) shl 8 or (_rowdata[13].toInt() and 0xFF)
            val pd7 = mPDSensor.RED_PD3
            last_value = now_value
            now_value = pd7

            if ((min_setting > 2) or (max_setting > 2)) {
                ir_max_value = now_value
                break
            }

            if (now_value < cal_v1 && last_value > cal_v1) {
                ir_max_value = now_value
                break
            }

            var isHigh = now_value > cal_v1
            if (isHigh) {
                led1.scale = 1
                led1.coarse = led1.coarse - 1
                if (led1.coarse < 0) {
                    led1.coarse = 0
                    min_setting++
                }
                //往下調整
            } else {
                //往上調整
                led1.scale = 1
                led1.coarse = led1.coarse + 1
                if (led1.coarse > 15) {
                    led1.coarse = 15
                    max_setting++
                }
            }

            writeCmdToCharacteristic(mBluetoothWrite, getLedCoarseCmdByteArray(led1))
            writeCmdToCharacteristic(mBluetoothWrite, getLedFineCmdByteArray(led1))
            delay(1000L)
            result = try {
                mBluetoothGatt.readCharacteristic(mBluetoothRead)
            } catch (e: GattConnectionLost) {
                return false
            }
        }

        last_value = 0 //3300
        now_value = 0 //3300
        min_setting = 0
        max_setting = 0

        Log.d("ble", "start config ir pd")
        while (true) {
            var _rowdata = result.value
            var mPDSensor = BleDeviceChartViewModel.PDSensor(_rowdata)
//            val pd3 = (_rowdata[4].toInt() and 0xFF) shl 8 or (_rowdata[5].toInt() and 0xFF)
            val pd3 = mPDSensor.IR_PD3
            last_value = now_value
            now_value = pd3

            if ((min_setting > 2) or (max_setting > 2)) {
                break
            }

            if (now_value < ir_max_value && last_value > ir_max_value) {
                break
            }

            var isHigh = now_value > ir_max_value
            if (isHigh) {
                led2.scale = 0
                led2.coarse = led2.coarse - 1
                if (led2.coarse < 0) {
                    led2.coarse = 0
                    min_setting++
                }
                //往下調整
            } else {
                //往上調整
                led2.scale = 0
                led2.coarse = led2.coarse + 1
                if (led2.coarse > 15) {
                    led2.coarse = 15
                    max_setting++
                }
            }

            mBluetoothGatt.writeCharacteristicOrThrow(
                mBluetoothWrite,
                getLedCoarseCmdByteArray(led2)
            )
            mBluetoothGatt.writeCharacteristicOrThrow(mBluetoothWrite, getLedFineCmdByteArray(led2))
            delay(1000L)
            result = try {
                mBluetoothGatt.readCharacteristic(mBluetoothRead)
            } catch (e: GattConnectionLost) {
                return false
            }
        }
        return true
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_linechart)

        mViewModel = ViewModelProviders.of(this).get(BleDeviceChartViewModel::class.java)

        title = "Monitor"

        mContext = this
        bluetoothDevice = intent.getParcelableExtra("device")

        mStopButton = findViewById(R.id.button_stopAndUp)

        aaChartView = findViewById(R.id.AAChartView)

        mSelectChartView = findViewById(R.id.spinner_charttype)


        val adapter = ArrayAdapter.createFromResource(
            this, R.array.chart_class,
            android.R.layout.simple_list_item_1
        )

        mSelectChartView.adapter = adapter
        mSelectChartView.setSelection(mViewModel.ChartType_SMO2, true)

        isconnect = false

        if (bluetoothDevice.address.equals("58:93:D8:49:14:CD")
            || bluetoothDevice.address.equals("58:93:D8:49:13:C5")
            || bluetoothDevice.address.equals("58:93:D8:49:12:E6"))
        {
            cal_v1 = 4000
        }else if( bluetoothDevice.address.equals("58:93:D8:49:13:86")){
            cal_v1 = 3500
        }else if( bluetoothDevice.address.equals("58:93:D8:49:14:AD")){
            cal_v1 = 5000
        }else{
            cal_v1 = 5500
        }

        setUpAAChartView()
    }

    override fun onResume() {
        super.onResume()

        mSelectChartView.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onNothingSelected(p0: AdapterView<*>?) {}

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, type: Int, p3: Long) {

                GlobalScope.launch(Dispatchers.Main) {
                    changeChart.set(true)
                    delay(100)
                    mViewModel.mChartType = type
                    updateAAChartView(aaChartModel)
                    changeChart.set(false)
                }
            }
        }

        showInfoDailog()

        mStopButton.setOnClickListener {
            GlobalScope.launch {

                mBluetoothGatt.disconnect()
                mViewModel.stopSaveSmo2()
                mViewModel.uploadSmo2ToCloud()
            }
        }

        mViewModel.showUploadDailog.observe(this, Observer {
            MaterialDialog(mContext).show {
                message(text = "上傳完畢")
                positiveButton(text = "確定") {
                    finish()
                }
            }
        })
    }

    override fun onPause() {
        super.onPause()
        GlobalScope.launch {
            mBluetoothGatt.disconnect()
            mViewModel.closeFile()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        connectJod.cancel()
        notifyJod.cancel()
    }

    fun setUpAAChartView() {
        aaChartModel = configureAAChartModel()
        aaChartModel = changeAAChartViewType(aaChartModel, mViewModel.ChartType_SMO2)
        val aaOptions: AAOptions = AAOptionsConstructor.configureChartOptions(aaChartModel)
        aaChartView?.aa_drawChartWithChartOptions(aaOptions)
    }

    private fun configureAAChartModel(): AAChartModel {
        val chartModel = configureChartBasicContent()
        chartModel.series(
            arrayOf(
                AASeriesElement()
                    .name("SMO2")
                    .lineWidth(1f)
                    .data(mViewModel.smo2Data.toTypedArray())
            )
        )
        return chartModel
    }

    private fun configureChartBasicContent(): AAChartModel {
        return AAChartModel()
            .chartType(AAChartType.Spline)
            .dataLabelsEnabled(false)
            .touchEventEnabled(false)
//            .yAxisGridLineWidth(0f)
            .title("")
            .yAxisTitle("")
            .yAxisLineWidth(1f)
            .markerEnabled(false)
            .colorsTheme(
                arrayOf(
                    "#FF0000",
                    "#FF8800",
                    "#8B4513",
                    "#00FF00",
                    "#0000FF",
                    "#7700FF",
                    "#FF00FF",
                    "#000000"
                )
            )
            .stacking(AAChartStackingType.False)
    }

    private fun changeAAChartViewType(chartModel: AAChartModel, type: Int): AAChartModel {
        return when (type) {
            mViewModel.ChartType_PD -> chartModel.yAxisMax(null).yAxisMin(null)
            mViewModel.ChartType_SMO2 -> chartModel.yAxisMax(110f).yAxisMin(-10f)
            mViewModel.ChartType_EMG -> chartModel.yAxisMax(2000f).yAxisMin(0f)
            else -> chartModel.yAxisMax(null).yAxisMin(null)
        }
    }

    fun updateAAChartView(chartModel: AAChartModel) {

        aaChartModel.series(mViewModel.configAAChartViewData())
        aaChartModel = changeAAChartViewType(aaChartModel, mViewModel.mChartType)

        val aaOptions: AAOptions = AAOptionsConstructor.configureChartOptions(chartModel)
        aaChartView!!.aa_drawChartWithChartOptions(aaOptions)
    }

    private fun updateView(_rowdata: ByteArray) {
        mViewModel.saveData(_rowdata)

        if (!changeChart.get())
            GlobalScope.launch(Dispatchers.Main) {
//                Log.i("Gesture", "updateView")
                aaChartView!!.aa_onlyRefreshTheChartDataWithChartOptionsSeriesArray(
                    mViewModel.configAAChartViewData()
                    , false
                )
            }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        var led: LEDSetting
        when (item.itemId) {
            R.id.setting_led1 -> led = mLedSettings.get(0)
            R.id.setting_led2 -> led = mLedSettings.get(1)
            else -> led = mLedSettings.get(0)
        }

        val dialog = MaterialDialog(this)
            .title(text = led.name + " Setting")
            .customView(R.layout.dialog_custom_view)

        val view = dialog.getCustomView()

        val coarse_text = view.findViewById<TextView>(R.id.textview_c)

        val scale = view.findViewById<Spinner>(R.id.spinner_scale)
        val scaleadapter = ArrayAdapter.createFromResource(
            this, R.array.led_scale,
            android.R.layout.simple_list_item_1
        )
        scale.adapter = scaleadapter
        scale.setSelection(led.scale)

        val coarse = view.findViewById<Spinner>(R.id.spinner_coarse)
        val coarseadapter = ArrayAdapter.createFromResource(
            this, R.array.led_coarse,
            android.R.layout.simple_list_item_1
        )
        coarse.adapter = coarseadapter
        coarse.setSelection(led.coarse)

        val seekbar = view.findViewById<SeekBar>(R.id.seekbar_final)
        seekbar.max = 31
        seekbar.progress = led.fine

        coarse.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onNothingSelected(p0: AdapterView<*>?) {}

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, v: Int, p3: Long) {
//                Log.e("scale", "coarse: $v")
                showCurrent(
                    coarse_text,
                    coarse.selectedItemPosition,
                    scale.selectedItemPosition,
                    seekbar.progress
                )
            }
        }

        scale.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onNothingSelected(p0: AdapterView<*>?) {}

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, v: Int, p3: Long) {
//                Log.e("scale", "scale: $v")
                showCurrent(
                    coarse_text,
                    coarse.selectedItemPosition,
                    scale.selectedItemPosition,
                    seekbar.progress
                )
            }
        }
        seekbar.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {

            override fun onProgressChanged(p0: SeekBar?, value: Int, p2: Boolean) {
//                Log.e("scale", "onProgressChanged: $value")
                showCurrent(
                    coarse_text,
                    coarse.selectedItemPosition,
                    scale.selectedItemPosition,
                    seekbar.progress
                )
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {
            }

            override fun onStopTrackingTouch(p0: SeekBar?) {
            }
        })

        showCurrent(coarse_text, led.coarse, led.scale, led.fine)

        dialog.show {
            positiveButton(text = "SAVE") {
                led.coarse = coarse.selectedItemPosition
                led.scale = scale.selectedItemPosition
                led.fine = seekbar.progress

                var led_cmd_coarse_bytes = ByteArray(4)
                when (item.itemId) {
                    R.id.setting_led1 -> led_cmd_coarse_bytes.writeInt8(0x23, 1)
                    R.id.setting_led2 -> led_cmd_coarse_bytes.writeInt8(0x24, 1)
                }

                var coarse_cmd = (0x1031 and 0xFFF0) xor (led.coarse shl 0) xor (led.scale shl 13)
                led_cmd_coarse_bytes.writeInt16BE(coarse_cmd, 2)

                val led1 = mLedSettings.get(0)
                val led2 = mLedSettings.get(1)
                var led_cmd_fine_bytes = ByteArray(4)

                led_cmd_fine_bytes.writeInt8(0x25, 1)

                var fine_cmd = (0x0451 and 0xFFE0) xor (led1.fine shl 0)
                fine_cmd = (fine_cmd and 0xF83F) xor (led2.fine shl 6)
                led_cmd_fine_bytes.writeInt16BE(fine_cmd, 2)

                GlobalScope.launch {
                    writeCmdToCharacteristic(mBluetoothWrite, led_cmd_coarse_bytes)
                    writeCmdToCharacteristic(mBluetoothWrite, led_cmd_fine_bytes)
                }
            }
            negativeButton(text = "CANCEL") {
            }
        }
        return true
    }


    fun showInfoDailog() {
        val dialog = MaterialDialog(this)
            .title(text = "請輸入基本資料:")
            .customView(R.layout.activity_setinfo)

        dialog.show {
            positiveButton(text = "開始") {
                val view = it.getCustomView()

                var userIdView = view.findViewById<EditText>(R.id.editText_UserId)
                var zoneView = view.findViewById<EditText>(R.id.editText_zone)
                mViewModel.setUserInfomation(
                    userIdView.text.toString().toInt(),
                    zoneView.text.toString()
                )

                GlobalScope.launch {
                    var result = connectAndConfig(mContext, bluetoothDevice)
//                    Log.i("Gesture", "connectAndConfig: " + result)
                    if (result)
                        mViewModel.startSaveSmo2()
                }
            }
        }
    }

    fun getLedCoarseCmdByteArray(led: LEDSetting): ByteArray {

        var led_cmd_coarse_bytes = ByteArray(4)
        when (led.number) {
            1 -> led_cmd_coarse_bytes.writeInt8(0x23, 1)
            2 -> led_cmd_coarse_bytes.writeInt8(0x24, 1)
        }
        var coarse_cmd = (0x1031 and 0xFFF0) xor (led.coarse shl 0) xor (led.scale shl 13)
        led_cmd_coarse_bytes.writeInt16BE(coarse_cmd, 2)
        return led_cmd_coarse_bytes
    }

    fun getLedFineCmdByteArray(led: LEDSetting): ByteArray {
        val led1 = mLedSettings.get(0)
        val led2 = mLedSettings.get(1)
        var led_cmd_fine_bytes = ByteArray(4)
        led_cmd_fine_bytes.writeInt8(0x25, 1)
        var fine_cmd = (0x0451 and 0xFFE0) xor (led1.fine shl 0)
        fine_cmd = (fine_cmd and 0xF83F) xor (led2.fine shl 6)
        led_cmd_fine_bytes.writeInt16BE(fine_cmd, 2)
        return led_cmd_fine_bytes
    }


    private fun showCurrent(view: TextView, c: Int, s: Int, f: Int) {

        val coarse = 50.3 + 19.8 * c
        val scale = 0.1 + 0.9 * s
        val fine = 0.74 + 0.022 * f
        val pink = coarse * scale * fine

        view.text = "%.1f mA".format(pink)
    }
}

private fun ByteArray.toHex(): String {
    return joinToString("") { "%02x".format(it) }
}

fun byteArrayOfInts(vararg ints: Int) = ByteArray(ints.size) { pos -> ints[pos].toByte() }

