package com.bkeen.smo2monitor

import android.util.Log
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.ConnectionPool
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.*
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST
import java.util.concurrent.TimeUnit

interface Smo2Api {

    @POST("/Humon.php")
    suspend fun post(@Body body: Smo2Request): Response<Smo2Resp>

}

object HttpService {

    val smo2Api: Smo2Api
    private val hosturl = "http://drought0621dev.wndc.nkust.edu.tw"


    init {
        smo2Api = createRetrofit(createOkHttpClient(), hosturl)
    }

    private fun createOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .connectTimeout(60L, TimeUnit.SECONDS)
            .readTimeout(60L, TimeUnit.SECONDS)
            .connectionPool(ConnectionPool(0, 1, TimeUnit.NANOSECONDS))
//            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .build()
    }

    private fun createRetrofit(okHttpClient: OkHttpClient, serverUrl: String): Smo2Api {

        val retrofit = Retrofit.Builder()
            .baseUrl(serverUrl)                                     //設定請求URL
            .client(okHttpClient)                                   //設定OkHttp攔截器
            .addConverterFactory(GsonConverterFactory.create())     //設定解析工具，這裡使用Gson解析，你也可以使用Jackson等
            .build()

        return retrofit.create(Smo2Api::class.java)
    }

    suspend fun updateSmo2Data(smo2data: Smo2Request):Boolean{

        val result = withContext(Dispatchers.IO){smo2Api.post(smo2data)}
//        Log.d("HttpService","return :" + result.raw().toString())
        if(result.isSuccessful){
            val smo2Resp = result.body()!!
//            Log.d("HttpService","return :" + smo2Resp.State)
        }
        return result.isSuccessful
    }



}