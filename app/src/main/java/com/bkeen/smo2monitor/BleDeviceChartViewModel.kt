package com.bkeen.smo2monitor

import android.os.Environment
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.aachartmodel.aainfographics.AAInfographicsLib.AAChartCreator.AASeriesElement
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStreamWriter
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class BleDeviceChartViewModel : ViewModel() {

    var smo2Data = ArrayList<Double>()
    var emgData = ArrayList<Double>()
    var irPd1Data = ArrayList<Double>()
    var irPd2Data = ArrayList<Double>()
    var irPd3Data = ArrayList<Double>()
    var irPd4Data = ArrayList<Double>()
    var redPd1Data = ArrayList<Double>()
    var redPd2Data = ArrayList<Double>()
    var redPd3Data = ArrayList<Double>()
    var redPd4Data = ArrayList<Double>()

    internal var count = 0f

    private var mFilterCount: Int = 20

    var ChartType_PD = 0
    var ChartType_SMO2 = 1
    var ChartType_EMG = 2

    internal var LOG_PATH =
        Environment.getExternalStorageDirectory().toString() + File.separator + "Smo2Log"
    private lateinit var mFile: File
    private lateinit var myOutWriter: OutputStreamWriter
    private lateinit var fOut: FileOutputStream
    private var mFilter: MedianFilter = MedianFilter()
    val mainHandler = Handler(Looper.getMainLooper())

    private var emg_rowdata: ArrayList<Int>
    private var smo2_clouddata: ArrayList<String>
    private var hgb_clouddata: ArrayList<String>
    private var posixtime_clouddata: ArrayList<String>
    private var timer_clouddata: ArrayList<String>

    private var mUserId: Int = 0
    private lateinit var mZoneId: String
    private var mTimer: Int = 0

    private lateinit var smo2Request: Smo2Request

    private val _showUploadDailog = SingleLiveEvent<Any>()

    val showUploadDailog: LiveData<Any>
        get() = _showUploadDailog

    private val _showUploaProcessDailog = SingleLiveEvent<Any>()

    val showUploaProcessDailog: LiveData<Any>
        get() = _showUploaProcessDailog

    class PDSensor constructor(rowdata: ByteArray) {

        var IR_PD1: Int = 0
        var IR_PD2: Int = 0
        var IR_PD3: Int = 0
        var IR_PD4: Int = 0
        var RED_PD1: Int = 0
        var RED_PD2: Int = 0
        var RED_PD3: Int = 0
        var RED_PD4: Int = 0

        init {
            var inedx = 0
            for (i in 0..7) {
                var value =
                    (rowdata[inedx].toInt() and 0xFF) shl 8 or (rowdata[inedx + 1].toInt() and 0xFF)
//                var value = rowdata.getUInt16(i)
                setPDValueofIndex(i, value)
                inedx += 2
            }
        }

        fun getPDValueofIndex(num: Int): Int {
            when (num) {
                0 -> return IR_PD1
                1 -> return IR_PD2
                2 -> return IR_PD3
                3 -> return IR_PD4
                4 -> return RED_PD1
                5 -> return RED_PD2
                6 -> return RED_PD3
                7 -> return RED_PD4
                else -> return IR_PD1
            }

        }

        private fun setPDValueofIndex(num: Int, value: Int) {
            when (num) {
                0 -> IR_PD1 = value
                1 -> IR_PD2 = value
                2 -> IR_PD3 = value
                3 -> IR_PD4 = value
                4 -> RED_PD1 = value
                5 -> RED_PD2 = value
                6 -> RED_PD3 = value
                7 -> RED_PD4 = value
                else -> IR_PD1 = value
            }
        }

        override fun toString(): String {
            return "" + IR_PD1 + "\t" + IR_PD2 + "\t" + IR_PD3 + "\t" + IR_PD4 + "\t" + RED_PD1 + "\t" + RED_PD2 + "\t" + RED_PD3 + "\t" + RED_PD4
        }
    }

    var mChartType: Int

    init {
        emg_rowdata = ArrayList()
        for (i in 0..mFilterCount)
            emg_rowdata.add(0)

        smo2_clouddata = ArrayList()
        hgb_clouddata = ArrayList()
        posixtime_clouddata = ArrayList()
        timer_clouddata = ArrayList()
        mChartType = ChartType_SMO2

    }

    fun saveData(_rowdata: ByteArray) {
        var mPDSensor = PDSensor(_rowdata)
//        Log.d("saveData",mPDSensor.toString())

        irPd1Data.add(mPDSensor.getPDValueofIndex(0).toDouble())
        irPd2Data.add(mPDSensor.getPDValueofIndex(1).toDouble())
        irPd3Data.add(mPDSensor.getPDValueofIndex(2).toDouble())
        irPd4Data.add(mPDSensor.getPDValueofIndex(3).toDouble())
        redPd1Data.add(mPDSensor.getPDValueofIndex(4).toDouble())
        redPd2Data.add(mPDSensor.getPDValueofIndex(5).toDouble())
        redPd3Data.add(mPDSensor.getPDValueofIndex(6).toDouble())
        redPd4Data.add(mPDSensor.getPDValueofIndex(7).toDouble())

//        Log.d("saveData",redPd4Data.joinToString(":"))

        if (irPd1Data.size == BleDeviceChartActivity.data_count) {
            irPd1Data.removeAt(0)
            irPd2Data.removeAt(0)
            irPd3Data.removeAt(0)
            irPd4Data.removeAt(0)
            redPd1Data.removeAt(0)
            redPd2Data.removeAt(0)
            redPd3Data.removeAt(0)
            redPd4Data.removeAt(0)
        }


        var smo2 = calculateSmo2(mPDSensor).toFloat()
//        val smo2set = smo2LineData.getDataSetByIndex(0)

        emg_rowdata.add(smo2.toInt())

        if (emg_rowdata.size > mFilterCount) {
            emg_rowdata.removeAt(0)
        }

        smo2 = mFilter.findLocalMedian(emg_rowdata.toIntArray(), 0, mFilterCount).toFloat()

//        smo2set!!.addEntry(Entry(count, smo2))
        smo2Data.add(smo2.toDouble())

        if (smo2Data.size == BleDeviceChartActivity.data_count) {
//            smo2set.removeFirst()
            smo2Data.removeAt(0)
        }

//        val emgset = emgLineData.getDataSetByIndex(0)
        var emg = _rowdata.getUInt32(16)
//        emgset!!.addEntry(Entry(count, emg.toFloat()))
        emgData.add(emg.toDouble())
        hgb_clouddata.add(emg.toDouble().toString())
        if (emgData.size == BleDeviceChartActivity.data_count) {
//            emgset.removeFirst()
            emgData.removeAt(0)
        }

        count++

        writeToFile(mPDSensor, smo2, emg)
    }


    fun configAAChartViewData(): Array<AASeriesElement> {
        return when (mChartType) {
            ChartType_PD -> arrayOf(
                AASeriesElement()
                    .name("IR_PD1")
                    .lineWidth(1f)
                    .data(
                        irPd1Data.toTypedArray()
                    ), AASeriesElement()
                    .name("IR_PD2")
                    .lineWidth(1f)
                    .data(
                        irPd2Data.toTypedArray()
                    ), AASeriesElement()
                    .name("IR_PD3")
                    .lineWidth(1f)
                    .data(
                        irPd3Data.toTypedArray()
                    ), AASeriesElement()
                    .name("IR_PD4")
                    .lineWidth(1f)
                    .data(
                        irPd4Data.toTypedArray()
                    ), AASeriesElement()
                    .name("RED_PD1")
                    .lineWidth(1f)
                    .data(
                        redPd1Data.toTypedArray()
                    ), AASeriesElement()
                    .name("RED_PD2")
                    .lineWidth(1f)
                    .data(
                        redPd2Data.toTypedArray()
                    ), AASeriesElement()
                    .name("RED_PD3")
                    .lineWidth(1f)
                    .data(
                        redPd3Data.toTypedArray()
                    ), AASeriesElement()
                    .name("RED_PD4")
                    .lineWidth(1f)
                    .data(
                        redPd4Data.toTypedArray()
                    )
            )
            ChartType_SMO2 -> arrayOf(
                AASeriesElement()
                    .name("SMO2")
                    .lineWidth(1f)
                    .data(
                        smo2Data.toTypedArray()
                    )
            )
            ChartType_EMG -> arrayOf(
                AASeriesElement()
                    .name("EMG")
                    .lineWidth(1f)
                    .data(
                        emgData.toTypedArray()
                    )
            )
            else -> arrayOf(
                AASeriesElement()
                    .name("smo2")
                    .lineWidth(1f)
                    .data(
                        smo2Data.toTypedArray()
                    )
            )
        }

    }

    fun startSaveSmo2() {
        mainHandler.post(object : Runnable {
            override fun run() {
                mTimer++
                smo2_clouddata.add(smo2Data.last().toString())
                posixtime_clouddata.add(Date().time.toString())
                timer_clouddata.add(mTimer.toString())

//                Log.d("TEST", smo2_clouddata.joinToString(":"))
                mainHandler.postDelayed(this, 1000)
            }
        })
    }

    fun stopSaveSmo2() {
        mainHandler.removeCallbacksAndMessages(null)
    }

    fun uploadSmo2ToCloud() {

        GlobalScope.launch(Dispatchers.Main) {
            _showUploaProcessDailog.call()
        }


        var zoneIds = ArrayList<String>()
        zoneIds.add(mZoneId)

        val date = SimpleDateFormat("yyyy-MM-dd").format(Date())
        val body =
            Smo2Request(
                mUserId,
                0,
                date,
                posixtime_clouddata,
                timer_clouddata,
                hgb_clouddata,
                smo2_clouddata,
                zoneIds
            )

        GlobalScope.launch {
            var result = HttpService.updateSmo2Data(body)
            if (result) {
                launch(Dispatchers.Main) {
                    _showUploadDailog.call()
                }
            }

        }
    }

    private fun calculateSmo2(sensor: PDSensor): Double {

        var OD740_In = 65535
        var OD850_In = 65535

        var pd1 = sensor.IR_PD1.toDouble()
        var pd2 = sensor.IR_PD2.toDouble()
        var pd3 = sensor.IR_PD3.toDouble()
        var pd5 = sensor.RED_PD1.toDouble()
        var pd6 = sensor.RED_PD2.toDouble()
        var pd7 = sensor.RED_PD3.toDouble()

        var OD1_740 = Math.log10(pd7 / OD740_In) - Math.log10(pd6 / OD740_In)
        var OD2_740 = Math.log10(pd5 / OD740_In) - Math.log10(pd6 / OD740_In)
        var OD1_850 = Math.log10(pd3 / OD850_In) - Math.log10(pd2 / OD850_In)
        var OD2_850 = Math.log10(pd1 / OD850_In) - Math.log10(pd2 / OD850_In)
        var smo2 =
            (70 * (OD2_740 / OD2_850) * (OD2_740 / OD2_850)) + (-1.66 * (OD1_740 / OD1_850)) + 1.8
        return if (smo2 > 100) 100.0 else smo2
    }


    fun setUserInfomation(userId: Int, zone_id: String) {
        this.mUserId = userId
        this.mZoneId = zone_id
    }

    fun createFile() {
        try {
            Log.e("Exception", "createFile")
            val simpleDateFormat = SimpleDateFormat("yyyyMMdd_HHmmss")
            val date = simpleDateFormat.format(Date())

            val path = File(LOG_PATH)
            if (!path.exists()) {
                Log.e("Exception", "mkdir:" + path.mkdir())
            }

            mFile = File(LOG_PATH, date + ".txt")
            mFile.createNewFile()
            fOut = FileOutputStream(mFile)
            myOutWriter = OutputStreamWriter(fOut)
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    fun closeFile() {
        try {
            Log.e("Exception", "closeFile")
            myOutWriter.close()
            fOut.flush()
            fOut.close()
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    fun writeToFile(pd: PDSensor, smo2: Float, emg: Int) {
        try {
//            Log.e("Exception", "writeToFile")
            var str = System.currentTimeMillis().toString() + "\t"
            str += pd.toString() + "\t" + smo2 + "\t" + emg + "\r\n"
            myOutWriter.append(str)
        } catch (e: IOException) {
            Log.e("Exception", "File write failed: $e")
        }

    }

    private fun ByteArray.toHex(): String {
        return joinToString("") { "%02x".format(it) }
    }

    fun ByteArray.getUInt32(index: Int): Int {
        if (index > (this.size - 4))
            return 0
        else
            return (this[index].toInt() and 0xFF) shl 24 or
                    (this[index + 1].toInt() and 0xFF) shl 16 or
                    (this[index + 2].toInt() and 0xFF) shl 8 or
                    (this[index + 3].toInt() and 0xFF)
    }


}