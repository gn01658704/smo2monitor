package com.bkeen.smo2monitor

import android.Manifest
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanFilter
import android.bluetooth.le.ScanResult
import android.bluetooth.le.ScanSettings
import android.content.Intent
import android.os.Bundle
import android.os.ParcelUuid
import android.view.Menu
import android.view.View
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.AdapterView.OnItemClickListener
import android.widget.ArrayAdapter
import android.widget.ListView


import java.util.ArrayList

import androidx.appcompat.app.AppCompatActivity
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import android.Manifest.permission
import android.Manifest.permission.RECORD_AUDIO
import android.Manifest.permission.READ_CONTACTS
import android.os.Environment
import android.util.Log
import com.karumi.dexter.Dexter
import com.karumi.dexter.listener.PermissionRequest
import java.io.File


class MainActivity : AppCompatActivity(), BluetoothAdapter.LeScanCallback, OnItemClickListener {
    private var adapter: ArrayAdapter<String>? = null
    private val bluetoothDeviceArrayList = ArrayList<BluetoothDevice>()
    private val mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
    internal var bluetoothLeScanner = mBluetoothAdapter!!.bluetoothLeScanner
    internal var settings = ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY).build()
    private val arrayList = ArrayList<String>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_main)
        title = "Select sensor"

        val listView = findViewById<ListView>(R.id.listView1)
        adapter = ArrayAdapter(
            this@MainActivity,
            android.R.layout.simple_list_item_1, arrayList
        )
        listView.adapter = adapter
        listView.onItemClickListener = this

        if (mBluetoothAdapter != null && !mBluetoothAdapter.isEnabled) {
            val enableBtIntent = Intent(
                BluetoothAdapter.ACTION_REQUEST_ENABLE
            )
            startActivityForResult(enableBtIntent, 1)
        }

        Dexter.withActivity(this)
            .withPermissions(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ).withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {/* ... */
                }

                override fun onPermissionRationaleShouldBeShown(
                    permissions: List<PermissionRequest>,
                    token: PermissionToken
                ) {/* ... */
                    token?.continuePermissionRequest()
                }
            }).check()

    }

    override fun onResume() {
        super.onResume()
        Log.e("onResume", "Path:" + Environment.getExternalStorageDirectory().toString() + File.separator + "Smo2Log")


        val filters = ArrayList<ScanFilter>()
        val filter =
            ScanFilter.Builder().setServiceUuid(ParcelUuid.fromString("0000fff0-0000-1000-8000-00805f9b34fb")).build()
        filters.add(filter)

        bluetoothLeScanner.startScan(filters, settings, object : ScanCallback() {

            override fun onScanResult(callbackType: Int, result: ScanResult) {
                super.onScanResult(callbackType, result)
                val device = result.device
                if (!bluetoothDeviceArrayList.contains(device) && device.name != null) {

                    bluetoothDeviceArrayList.add(device)
                    val name = device.name
                    arrayList.add(name)

                    adapter!!.notifyDataSetChanged()
                }
            }

            override fun onScanFailed(errorCode: Int) {
                super.onScanFailed(errorCode)
            }
        })


    }

    override fun onLeScan(bluetoothDevice: BluetoothDevice, i: Int, bytes: ByteArray) {

    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return false
    }

    override fun onItemClick(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {

        val bluetoothDevice = bluetoothDeviceArrayList[i]
        if (bluetoothDevice != null) {
            bluetoothLeScanner.stopScan(object : ScanCallback() {
                override fun onScanResult(callbackType: Int, result: ScanResult) {
                    super.onScanResult(callbackType, result)
                }
            })

            val intent = Intent(this, BleDeviceChartActivity::class.java)
            intent.putExtra("device", bluetoothDevice)
            startActivity(intent)
        }
    }


}
