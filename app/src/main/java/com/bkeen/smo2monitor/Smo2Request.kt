package com.bkeen.smo2monitor

data class Smo2Request(
    var User_id: Int,
    var exercise_th_id: Int,
    var date: String,
    var POSIX_Time: List<String>,
    var Time: List<String>,
    var hgb: List<String>,
    var smo2: List<String>,
    var zone: List<String>
)