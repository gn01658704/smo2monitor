package com.bkeen.smo2monitor

data class Smo2Resp(
    val State: String,
    val Status: Boolean
)